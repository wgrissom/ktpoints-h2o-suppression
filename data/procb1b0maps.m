% Script to process Janni's B1/B0 maps for use in spectrally-selective
% kT-points pulse design

centerb1 = 0.2; % gauss, RF amplitude to set middle of brain to 

%%%%%%%%%%%%%%
% Phantom

% REDFLAG: Not implemented yet!

%%%%%%%%%%%%%%%%%%
% In vivo - ACC
[b0imgs,b0info] = loadPARREC('VC110_B0map_for_spokes_calculation_ACC_11_1');
[b1imgs,b1info] = loadPARREC('VC110_B1map_for_spokes_calculation_ACC_12_1');

% There are three slices
mapsacc.b0 = squeeze(b0imgs(:,:,:,end,end));

b1imgs = squeeze(b1imgs);
% first index, 4th dim is magnitude images
% second index, 4th dim is (relative?) phase images
% third index, 4th dim is b1 amplitude
%b1phs = squeeze(b1imgs(:,:,:,2,1));
mapsacc.b1 = squeeze(b1imgs(:,:,:,3,end)); % there is only one coil?

% scale so the middle of the brain is at 0.2 Gauss
tmp = mapsacc.b1(51:71,51:71,2);
mapsacc.b1 = mapsacc.b1*centerb1/mean(tmp(:));

mapsacc.info = b1info;

% calculate a mask
tmp = b0imgs(:,:,:,1,1);
mapsacc.mask = tmp > 0.75*median(tmp(:));

%%%%%%%%%%%%%%%%%%
% In vivo - PFC
[b0imgs,b0info] = loadPARREC('VC110_B0map_for_spokes_calculation_PFC_4_1');
[b1imgs,b1info] = loadPARREC('VC110_B1map_for_spokes_calculation_PFC_5_1');

% There are three slices
mapspfc.b0 = squeeze(b0imgs(:,:,:,end,end));

b1imgs = squeeze(b1imgs);
% first index, 4th dim is magnitude images
% second index, 4th dim is (relative?) phase images
% third index, 4th dim is b1 amplitude
%b1phs = squeeze(b1imgs(:,:,:,2,1));
mapspfc.b1 = squeeze(b1imgs(:,:,:,3,end)); % there is only one coil?

% scale so the middle of the brain is at 0.2 Gauss
tmp = mapspfc.b1(51:71,51:71,2);
mapspfc.b1 = mapspfc.b1*centerb1/mean(tmp(:));

mapspfc.info = b1info;

% calculate a mask
tmp = b0imgs(:,:,:,1,1);
mapspfc.mask = tmp > 0.5*median(tmp(:));

%%%%%%%%%%%%%%%%%%%%%%
% Angulated in vivo
[b0imgs,b0info] = loadPARREC('V5202_B0map_for_spokes_calculation_12_1');
[b1imgs,b1info] = loadPARREC('V5202_B1map_for_spokes_calculation_13_1');

% There are three slices
mapsang.b0 = squeeze(b0imgs(:,:,:,end,end));

b1imgs = squeeze(b1imgs);
% first index, 4th dim is magnitude images
% second index, 4th dim is (relative?) phase images
% third index, 4th dim is b1 amplitude
%b1phs = squeeze(b1imgs(:,:,:,2,1));
mapsang.b1 = squeeze(b1imgs(:,:,:,3,end)); % there is only one coil?

% scale so the middle of the brain is at 0.2 Gauss
tmp = mapsang.b1(51:71,51:71,2);
mapsang.b1 = mapsang.b1*centerb1/mean(tmp(:));

mapsang.info = b1info;

% calculate a mask
tmp = b0imgs(:,:,:,1,1);
mapsang.mask = tmp > 0.5*median(tmp(:));

save b1b0_umc maps* centerb1


