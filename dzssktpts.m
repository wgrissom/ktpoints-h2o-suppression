function [excpatterns, waveforms, sterr] = dzssktpts(p, maps)
  
% init section
Nb = length(p.fb); % Number of frequency bands

nthreads = p.nthreads;
computemethod = p.computemethod;

fov      = p.fov;  % Field of View in each dim, cm
dt = p.dt; % sampling period, seconds
dimxyz    = p.dimxyz; % pixels, dim of design grid
Ns = prod(dimxyz); % total # pixels
delta_tip = p.delta_tip; % flip angle, degrees
if delta_tip > 180 || delta_tip < 1
  error 'Please specify a tip angle between 1 and 180 degrees';
end
Np   = p.Npulses; % Number of (hard) subpulses
if abs(rem(Np,1)) > 0 || Np <= 0
  error 'Number of kT-points must be a positive integer';
end
beta = p.beta;
betaadjust = p.betaadjust;

ndims = p.ndims; % 2 or 3

%offcentre = p.offcentre; % volume centre shift, cm

Nsubpts = p.Nsubpts;
Nsubptsdc = p.Nsubptsdc;
nblippts = p.nblippts;

phaserelax = p.phaserelax; % relax target excitation phase 

ltgradopt = p.ltgradopt; % optimize large-tip gradient moments

maxpulsedur = p.maxpulsetime; % ms, maximum pulse duration

gmaxsys = p.maxgradamp; % maximum grad amp (g/cm)
gslew = p.maxgradslew; % maximum slew rate (g/cm/s)

filtertype = p.filtertype; % spectral filter type (for
                                % multiband designs)
  
trajres = p.trajres; % finest spatial res of k-encoding points to choose from

fb = p.fb; % frequency bands in Hz
famps = p.famps; % excitation amplitudes as a function of frequency

large_tip_flag = delta_tip > p.largetipthreshold;

if large_tip_flag
  disp 'Doing a large-tip-angle kT points design.';
end
    
gambar = 4257;             % gamma/2pi in Hz/g
gam = gambar*2*pi;         % gamma in radians/g
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Large tip settings
if Np <= 5
  betath = 10;
else
  betath = max(4,4+11-Np);
end
% beta threshold for large-tip design
% (smaller->faster, but may be less accurate). 
% I recommend betath = 10 for Nrungs <= 5, betath = 4
% for Nrungs > 5. Here we make a smooth transition from 10 to 4.
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load and interpolate b1 and b0 maps onto design grid
Nc = size(maps.b1,4); % # tx channels

% load B1 maps
B1in = repmat(maps.mask,[1 1 1 Nc]).*maps.b1;

% in case its not already done, subtract off first coil's phase
B1in = B1in.*exp(-1i*repmat(angle(B1in(:,:,:,1)),[1 1 1 Nc]));

if isfield(maps, 'b0')
  fmapin = maps.mask.*maps.b0;    
else 
  disp('Could not find b0 map, assuming ideal field');
  fmapin = zeros(dimxyz);
end

if ndims == 3
  % define output interpolation grid
  [xo,yo,zo] = meshgrid(-fov(1)/2:fov(1)/dimxyz(1):fov(1)/2-fov(1)/dimxyz(1),...
                        -fov(2)/2:fov(2)/dimxyz(2):fov(2)/2-fov(2)/dimxyz(2),...
                        -fov(3)/2:fov(3)/dimxyz(3):fov(3)/2-fov(3)/dimxyz(3));
  
  % interpolate B1 maps
  [xi,yi,zi] = meshgrid(-fov(1)/2:fov(1)/size(B1in,1):fov(1)/2-fov(1)/size(B1in,1),...
                        -fov(2)/2:fov(2)/size(B1in,2):fov(2)/2-fov(2)/size(B1in,2),...
                        -fov(3)/2:fov(3)/size(B1in,3):fov(3)/2-fov(3)/size(B1in,3));
  for ii = 1:Nc
    B1int(:,:,:,ii) = interp3(xi,yi,zi,B1in(:,:,:,ii),xo,yo,zo,'nearest',0);
  end
  
  % interpolate B0 map
  [xi,yi,zi] = meshgrid(fov(1)*[-1/2:1/size(fmapin,1):1/2-1/size(fmapin,1)],...
                        fov(2)*[-1/2:1/size(fmapin,2):1/2-1/size(fmapin,2)],...
                        fov(3)*[-1/2:1/size(fmapin,3):1/2-1/size(fmapin,3)]);
  fmapint = interp3(xi,yi,zi,fmapin,xo,yo,zo,'nearest',0);
  
  % get interpolated mask - nonzero where interpolated sens's are nonzero,
  % since we already masked sens's prior to interpolation
  maskint = sqrt(sum(abs(B1int).^2,4)) > 0;
elseif ndims == 2
  % define output interpolation grid
  [xo,yo] = meshgrid(-fov(1)/2:fov(1)/dimxyz(1):fov(1)/2-fov(1)/dimxyz(1),...
                     -fov(2)/2:fov(2)/dimxyz(2):fov(2)/2-fov(2)/dimxyz(2));
  
  % interpolate B1 maps
  [xi,yi] = meshgrid(-fov(1)/2:fov(1)/size(B1in,1):fov(1)/2-fov(1)/size(B1in,1),...
                     -fov(2)/2:fov(2)/size(B1in,2):fov(2)/2-fov(2)/size(B1in,2));
  for ii = 1:Nc
    B1int(:,:,ii) = interp2(xi,yi,B1in(:,:,ii),xo,yo,'nearest',0);
  end
  
  % interpolate B0 map
  [xi,yi] = meshgrid(fov(1)*[-1/2:1/size(fmapin,1):1/2-1/size(fmapin,1)],...
                     fov(2)*[-1/2:1/size(fmapin,2):1/2-1/size(fmapin,2)]);
  fmapint = interp2(xi,yi,fmapin,xo,yo,'nearest',0);
  
  % get interpolated mask - nonzero where interpolated sens's are nonzero,
  % since we already masked sens's prior to interpolation
  maskint = sqrt(sum(abs(B1int).^2,3)) > 0;  
end

sens = B1int;
%sens = flipdim(sens,1); % is this correct?
%sens = flipdim(sens,2);

mask = maskint;
%mask = flipdim(mask,1);
%mask = flipdim(mask,2);

fmap = fmapint;
%fmap = flipdim(fmap,1);
%fmap = flipdim(fmap,2);
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% design a slice-selective pulse
% on this gradient to init basis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

b1d = [ones(Nsubpts,1);zeros(nblippts,1)];
b1ddc = [ones(Nsubptsdc,1);zeros(nblippts,1)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get desired pattern for design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ndims == 3
    [xx,yy,zz] = ndgrid(-fov(1)/2:fov(1)/dimxyz(1):fov(1)/2-fov(1)/dimxyz(1), ...
        -fov(2)/2:fov(2)/dimxyz(2):fov(2)/2-fov(2)/dimxyz(2), ...
        -fov(3)/2:fov(3)/dimxyz(3):fov(3)/2-fov(3)/dimxyz(3));
    
    %  xx = xx + offcentre(1);
    %  yy = yy + offcentre(2);
    %  zz = zz + offcentre(3);
else
    [xx,yy] = ndgrid(-fov(1)/2:fov(1)/dimxyz(1):fov(1)/2-fov(1)/dimxyz(1), ...
        -fov(2)/2:fov(2)/dimxyz(2):fov(2)/2-fov(2)/dimxyz(2));
    
    %  xx = xx + offcentre(1);
    %  yy = yy + offcentre(2);
end

% desired excitation pattern
d = repmat(famps(:).',[Ns 1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mask b1 maps, target pattern 
% for design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sensd = reshape(sens,[Ns Nc]);
sensd = sensd(logical(mask(:)),:);
dd = d(logical(mask(:)),:);
xxd = xx(logical(mask(:)));
yyd = yy(logical(mask(:)));
if ndims == 3
  zzd = zz(logical(mask(:)));
  xyzd = [xxd yyd zzd];
else
  xyzd = [xxd yyd];
end
fmapd = fmap(logical(mask(:)));

if p.nb1scal > 1;
    % replicate all maps with multiple scalings of the b1 map
    b1scal = 1 + p.maxb1frac*(-1:2/(p.nb1scal-1):1);
    senstmp = [];
    for ii = 1:p.nb1scal
        senstmp = [senstmp;sensd*b1scal(ii)];
    end
    dd = repmat(dd,[p.nb1scal 1]);
    xyzd = repmat(xyzd,[p.nb1scal 1]);
    fmapd = repmat(fmapd,[p.nb1scal 1]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% design small-tip pulses
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% OMP pre-definitions
if ndims == 3
  dimsearch = 16;kfovsearch = 1/trajres;%8/fov;
  [kxs,kys,kzs] = ndgrid(-kfovsearch/2:kfovsearch/dimsearch:kfovsearch/2-kfovsearch/dimsearch);
  kxs = kxs(:);kys = kys(:);kzs = kzs(:);
  ks = [kxs kys kzs];
else
  dimsearch = 64;kfovsearch = 1/trajres;
  [kxs,kys] = ndgrid(-kfovsearch/2:kfovsearch/dimsearch:kfovsearch/2-kfovsearch/dimsearch);
  kxs = kxs(:);kys = kys(:);
  zi = dimsearch*(dimsearch/2) + dimsearch/2 + 1;
  kxs = [kxs(1:zi-1);kxs(zi+1:end)]; % remove DC as OMP candidate
  kys = [kys(1:zi-1);kys(zi+1:end)]; % remove DC as OMP candidate
  ks = [kxs kys];
end  
[rf,kpe,md,A,sterr,beta,phs,psb] = ilgreedylocal(dd*delta_tip*pi/180, ...
                                                 sensd,xyzd,ks,p.kmaxdistance,[],Np,fmapd,fb, ...
                                                 b1d,b1ddc,dt,beta,betaadjust,...
                                                 filtertype,phaserelax,[],[],mask, ...
                                                 computemethod,nthreads);      
                                             
                                             
b1dnew = {};
for ii = 1:Np
  b1dnew{ii} = b1d;
end
if strcmp(filtertype,'Lin phase')
  b1dnew{ceil(Np/2)} = b1ddc; % assumes we put one on more on the end with even number of spokes
elseif strcmp(filtertype,'Min phase')
  b1dnew{end} = b1ddc;
elseif strcmp(filtertype,'Max phase')
  b1dnew{1} = b1ddc;
end
b1d = b1dnew;

% embed resulting m into full array
m = zeros(Ns,Nb);
for ii = 1:Nb
  m(logical(mask(:)),ii) = md(:,ii);
end  
m = reshape(m,[dimxyz(:)' Nb]);

if large_tip_flag && Np > 1
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % define large-tip target patterns
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
  if delta_tip == 180
    ad = zeros(Ns,Nb);
  else
    ad = cos(delta_tip*pi/180/2)*abs(d);
  end
  bd = sin(delta_tip*pi/180/2)*abs(d);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % mask target patterns for design
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  add = ad(logical(mask(:)),:);
  bdd = bd(logical(mask(:)),:);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % set large tip beta by predicting
  % large tip error using small tip
  % results
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  lterrpred = 0;
  for ii = 1:Nb
    apred = cos(abs(m(:,:,:,ii))/2);apred = apred(logical(mask(:)));
    bpred = sin(abs(m(:,:,:,ii))/2);bpred = bpred(logical(mask(:)));
    lterrpred = lterrpred + 1/2*real((apred-add(:,ii))'*(apred-add(:,ii)) + ...
                                     (bpred-bdd(:,ii))'*(bpred-bdd(:,ii)));
  end
  % normalize large tip beta
  ltbeta = beta/sterr(2)*lterrpred;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % design large-tip pulses
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  z_tbw = -1;
  [rf,kpe,ad,bd,Blt,ltbeta,avgflip,phsa,psba,phsb,psbb] = ...
      lt_oc(add,bdd,sensd,xyzd,kpe,fmapd,...
      fb,b1d,z_tbw,[],[],dt,[],[],[],[],...
      ltbeta,betath,rf,p.ltphaserelax,[],[],[],[],...
      mask,computemethod,nthreads,ltgradopt);
  
  if dorflimit
        
    % see if we need to reduce RF mag
    rft = reshape(rf,[Np Nc]);
    maxrf = max(abs(rft),[],2);
    if any(maxrf > rflimit)
      keepgoing = 1;
    else
      keepgoing = 0;
    end
    
    % iteratively decrease gradients until either
    % RF limit is satisfied or pulse becomes too long
    while keepgoing

      % recalculate gradients and RF for each spoke
      Nttmp = 0;
      b1dnew = {};
      for ii = 1:Np
                
        % increase/decrease duration of each violating pulse to reduce/increase rf mag halfway down/up to 90% of rf limit
        b1dnew{ii} = [ones(ceil(maxrf(ii)*sum(b1d{ii})/(0.9*rflimit)),1);zeros(nblippts,1)];
        
        % rescale RF to generate same flips
        rft(ii,:) = rft(ii,:)*sum(b1d{ii})/sum(b1dnew{ii});
        
        Nttmp = Nttmp + length(b1dnew{ii});
        
      end
      
      % check total pulse duration
      pulsedurtmp = Nttmp*dt;
      if pulsedurtmp*1000 > maxpulsedur % pulse is too long
        disp(['RF limit violated, but adjusted pulse duration is too long.']) 
        keepgoing = 0;
      else % refine pulse
        disp('Adjusting subpulse durations.')
        b1d = b1dnew;
        % rescale rf regularization to be the same
        ltbeta = ltbeta*real(rf(:)'*rf(:))/real(rft(:)'*rft(:));
        % set rf equal to rf rescaled for new subpulses
        rf = rft(:);
        [rf,kpe,ad,bd,ltbeta,phsa,psba,phsb,psbb] = lt_mb_design_pe_oc_kt(add,bdd,sensd,xyzd,kpe,fmapd,...
                                                          fb,b1d,dt,ltbeta,betath,rf,phaserelax,phsa,psba,phsb,psbb,mask,computemethod,nthreads,ltgradopt);
        
        rft = reshape(rf,[Np Nc]);
        maxrf = max(abs(rft),[],2);
        if any(maxrf > rflimit)
          keepgoing = 1;
        else
          keepgoing = 0;
        end
      end
      
    end % while keepgoing
    
  end % if limit RF
  
  % embed resulting ad,bd into full arrays
  a = zeros(Ns,Nb);
  b = zeros(Ns,Nb);
  for ii = 1:Nb
    a(logical(mask(:)),ii) = ad(:,ii);
    b(logical(mask(:)),ii) = bd(:,ii);
  end
  
  a = reshape(a,[dimxyz(:)' Nb]);
  b = reshape(b,[dimxyz(:)' Nb]);
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get k, g for optimized traj
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 1:length(b1d)
  nrfpts(ii) = sum(abs(b1d{ii}) > 0);
end
[g,Npre,Npost] = compute_g_ktpoints(kpe,nrfpts,nblippts,gmaxsys,gslew,dt);

% strip off initial prewinder, since this is an exc pulse
%g = g(Npre+1:end,:);
%Npre = 0;

% get pulses to write out
%if large_tip_flag & Np > 1
%  B1wr = zeros(Ntdes,Nc);
%  for ii = 1:Nc
%    rungind = 1;
%    for jj = 1:Np
%      B1wr(rungind:rungind+length(Blt{jj})-1,ii) = Blt{jj}*rf((ii-1)*Np+jj);
%      rungind = rungind + length(Blt{jj});
%    end
%  end
%else
rf = reshape(rf,[Np Nc]);
B1wr = [];
for ii = 1:Np
  B1wr = [B1wr; b1d{ii}*rf(ii,:)];
end
%end

% add zeros for the spin-echo prewinder
B1wr = [zeros(Npre,Nc);B1wr];

% remove trailing zeros after rewinder
B1wr = B1wr(1:end-(nblippts-Npost),:);

fprintf('Peak digital RF magnitude: %0.2f\n',max(abs(B1wr(:))));
fprintf('Pulse duration: %0.2f ms\n',size(B1wr,1)*dt*1000);

excpatterns = m; 

waveforms.rf = B1wr;
waveforms.g = g;
waveforms.dt = dt;
  

