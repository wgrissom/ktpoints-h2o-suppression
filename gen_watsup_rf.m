rootFlipEnvelope = false;

% design a TB 4 water suppression pulse with a 200 Hz BW and 20 ms duration
load('rfworkspace','tbw','Nt');
[rf,b] = dzrf(256,tbw,'sat','ls',0.001,0.01);
rf = real(rf);
%rf = real(dzrf(256,tbw,'sat','ls',0.001,0.01));

if rootFlipEnvelope
    
    % root flip the envelope to minimize peak B1/SAR
    b = b./max(abs(freqz(b)));
    d1 = 0.001/2;
    b = b*sin(pi/2/2 + atan(d1*2)/2); % scale to target flip angle
    r = roots(b); % calculate roots of single-band beta polynomial
    % sort the roots by phase angle
    [~,idx] = sort(angle(r));
    r = r(idx);r = r(:);
    r = leja_fast(r);
    
    candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tbw/length(b)*pi;% ...
    
    rfall = [];
    for ii = 1:2^sum(candidates)
        
        % convert this ii to binary pattern
        doflip = de2bi(ii-1);
        % add zeros for the rest of the passbands
        doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
        % embed in all-roots vector
        tmp = zeros(length(b)-1,1);
        tmp(candidates) = doflip;
        doflip = tmp;
        % flip those indices
        rt = r(:);
        rt(doflip == 1) = conj(1./rt(doflip == 1));
        % get polynomial coefficients back from flipped roots
        tmp = poly(rt);
        % important: normalize *before* modulating! doesn't work
        % for negative shifted slices otherwise
        tmp = tmp./max(abs(freqz(tmp)));
        % store the rf
        rfall = [rfall col(b2rf(tmp(:)*sin(pi/2/2 + atan(d1*2)/2)))];
        
    end
    
    % take RF with min peak amplitude
    [~,ind] = min(max(abs(rfall),[],1));
    rf = rfall(:,ind);
    
end

% interpolate to 6.4 us dwell time
dt = 6.4e-3; % ms, dwell time of scanner
T = Nt*dt; % ms, total pulse duration
dtin = T/256;
rfwr = interp1((0:255)/255*T,rf,0:dt:T-dt,'spline',0);
% scale to same area as rf
rfwr = rfwr/sum(rfwr)*sum(rf); % radians

% Bloch-simulate over entire frequency range
load('rfworkspace','omdt','xx','sensBloch','garea','mask','sens','NfSim');
garea = [0*garea(:,1:2) garea(:,3)]; % zero out spiral grads, leave spectral 'gradient'
a = 1/(2*pi*4258*dt/1000*sum(rfwr)/max(abs(rfwr)))*(abs(sens(mask))'*abs(sens(mask)))^-1*sum(abs(sens(mask)))*pi/2;
[a,b] = blochsim_optcont_mex(ones(Nt,1),ones(Nt,1),complex(rfwr(:)./max(abs(rfwr))*a*dt/1000*4258*2*pi),sensBloch,...
                             garea,xx,omdt,2);
Mxy = embed(2*a.*b,repmat(mask,[1 1 NfSim]));
Mz = embed(1-2*abs(b).^2,repmat(mask,[1 1 NfSim]));

% write RF
rf2ppe('tailored_ex_rf.txt.spectral_watsup',length(rfwr)*dt,sum(rfwr)*180/pi,...
    length(rfwr)*dt,0,rfwr);

% write zero gradients
grwr = zeros(length(rfwr),3);
gr2ppe('tailored_ex_grads.txt.spectral_watsup',length(grwr)*dt,length(grwr)*dt,10*grwr);

% write out pulses for saturation acquisition
load('rfworkspace','rfHard','nHard','gCrush','nCrush','nDel'); % load hard pulse from immediately previous run of spiral_watsup.m
bsrf = load('~/code/bsrfopt/bsrf_21-Mar-2012.mat');
rfwr_crush = [rfwr(:);zeros(2*nDel+nCrush,1);rfHard*dt/1000*bsrf.nomb1*4257*2*pi];
gCrushAll = 10*[[grwr(:,1:2);zeros(nCrush+2*nDel+nHard,2)] ...
    [zeros(size(grwr,1),1);zeros(nDel,1);gCrush(:);zeros(nDel+nHard,1)]];
rfang = sum(rfwr_crush)*180/pi;
rf2ppe('tailored_ex_rf.txt.spectral_watsup_sat',length(rfwr_crush)*dt,rfang,...
    length(rfwr_crush)*dt,0,rfwr_crush);
% Also write out a zero pulse for reference
rfwr_crush_zero = [0*rfwr(:);zeros(2*nDel+nCrush,1);rfHard*dt/1000*bsrf.nomb1*4257*2*pi];
rfang = sum(rfwr_crush_zero)*180/pi;
rf2ppe('tailored_ex_rf.txt.spectral_watsup_sat_norf',length(rfwr_crush_zero)*dt,rfang,...
    length(rfwr_crush_zero)*dt,0,rfwr_crush_zero);
gr2ppe('tailored_ex_grads.txt.spectral_watsup_sat',size(gCrushAll,1)*dt,size(gCrushAll,1)*dt,gCrushAll);

% also make a hard pulse with the same peak amplitude to get Rx sensitivity
nHard = 35; % can't use root-flipped pulse to set nHard; this is about what we get for lin-phs pulse
%nHard = ceil(sum(rfwr)/max(abs(rfwr))/10); % 5 is fudge factor to get duration down
rfHard = ones(nHard,1);
rfHard = rfHard./sum(rfHard)*10/180*pi; % 10 degree flip

% write hard RF
rf2ppe('tailored_ex_rf.txt.hard',length(rfHard)*dt,sum(rfHard)*180/pi,...
    length(rfHard)*dt/2,0,rfHard);

% write zero gradients
grwr = zeros(length(rfHard),3);
gr2ppe('tailored_ex_grads.txt.hard',length(rfHard)*dt,length(rfHard)*dt/2,10*grwr);



