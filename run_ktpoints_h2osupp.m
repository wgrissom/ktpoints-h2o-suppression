addpath util/

mapselect = 'janni_ang'; % 'janni_ang', 'janni_pfc', 'janni_acc', 'vandy'
slind = 2; % index of slice - only works for 'janni_*'
params.Npulses = 40; % number of kT-points subpulses.
subpulsedur = 0.5; % ms, duration of each subpulse, including gradient blip
params.delta_tip = 90; % flip angle in deg
bw = 200; % Hz, width of passband
t = 20; % ms, duration of pulse
d1 = 0.01; % passband ripple (this is the target % flip angle error in water)
d2 = 0.01; % stopband ripple (this is the target % flip angle error outside of water)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set up the target pattern as a function of frequency
w = dinf(d1,d2)/bw/(t/1000); % fractional transition width
nf = params.Npulses; % number of points in our 'filter'
fedges = [0 (1-w)*(t/1000)*bw/2 (1+w)*(t/1000)*bw/2 nf/2]/(nf/2)/(subpulsedur/1000)/2; % frequency edges in Hz, normalized to Nyquist
% set up oversampled design freq grid
nf = 8*nf; % oversample the frequency grid by a factor of 8, compared to subpulse frequency
fb = (-nf/2:nf/2-1)/nf/(subpulsedur/1000); % frequencies in Hz
% remove frequencies outside our ROI
fmask = (fb > -fedges(2) & fb < fedges(2)) | fb > fedges(3);
fbm = fb(fmask);
params.fb = fbm;

% set up vector of target amplitudes
famps = double(fb > -fedges(2) & fb < fedges(2));
fampsm = famps(fmask);
params.famps = fampsm;

% passband/stopband ripple weights (REDFLAG: not yet implemented!!)
fwts = double(fb > -fedges(2) & fb < fedges(2)) + d1/d2*double(fb > fedges(3));
fwtsm = fwts(fmask);
params.fwts = fwtsm;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load b1/b0 maps
switch mapselect
    case 'vandy'
        load data/b1b0.mat;
    case 'janni_ang'
        tmp = load('data/b1b0_umc');
        maps = tmp.mapsang;
        maps.b1 = maps.b1(:,:,slind);
        maps.b0 = maps.b0(:,:,slind);
        maps.mask = maps.mask(:,:,slind);
        clear tmp
    case 'janni_acc'
        tmp = load('data/b1b0_umc');
        maps = tmp.mapsacc;
        maps.b1 = maps.b1(:,:,slind);
        maps.b0 = maps.b0(:,:,slind);
        maps.mask = maps.mask(:,:,slind);
        clear tmp
    case 'janni_pfc'
        tmp = load('data/b1b0_umc');
        maps = tmp.mapspfc;
        maps.b1 = maps.b1(:,:,slind);
        maps.b0 = maps.b0(:,:,slind);
        maps.mask = maps.mask(:,:,slind);
        clear tmp
end

maps.b0 = -maps.b0; % opposite dir of precession compared to GE

fovx = maps.info.imgdef.pixel_spacing_x_y.uniq(1) * size(maps.mask,1) / 10; % cm
fovy = maps.info.imgdef.pixel_spacing_x_y.uniq(2) * size(maps.mask,2) / 10;
fovz = 0;
%fovz = abs(info.imgdef.slice_gap_in_mm.uniq) * size(mask,3) / 10;

dimxyz = [32 32 1]; % dimensions of of design grid in (x,y,z)

offcentre = mean(maps.info.imgdef.image_offcentre_ap_fh_rl_in_mm.uniq/10,1); % center in cm
params.ndims = 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set up other design parameters
params.kmaxdistance = [Inf Inf];
params.pulse_func = 2; % 
params.dt = 6.4e-6; % dwell time of Philips system, s
params.Nsubpts = floor(subpulsedur/2/params.dt/1000); 
params.Nsubptsdc = floor(subpulsedur/2/params.dt/1000); 
params.nblippts = floor(subpulsedur/2/params.dt/1000); % number of time points between hard pulses to accomodate gradient blips
params.fov = [fovx fovy fovz];
params.dimxyz = dimxyz;%size(mask);
params.offcentre = offcentre; % cm, volume offset
params.largetipthreshold = Inf; 
params.maxpulsetime = Inf; % maximum pulse duration in ms
params.beta = 10^-1;  % RF Tikhonov regularization parameter
params.betaadjust = 0;
params.nthreads = 4; % number of compute threads (for mex only)
params.phaserelax = 'independent'; % type of target excitation phase relaxation
params.ltphaserelax = 'independent'; 
params.ltgradopt = 1; % do large-tip gradient optimization
params.maxgradamp = 4; % g/cm
params.maxgradslew = 14000; % g/cm/s
params.filtertype = 'Lin phase';
params.trajres = 6; % 2.87 at trajres = 1; % maximum spatial frequency of OMP search grid
params.computemethod = 'mex';

% REDFLAG: Not implemented!
params.nb1scal = 1;
params.maxb1frac = 0.25;

%matlabpool close force % close any open thread pools
%matlabpool(params.nthreads); % this will accelerate OMP a lot

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do the design!
[excpatterns, waveforms, sterr] = dzssktpts(params,maps);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% show the excitation pattern
% NOT IMPLEMENTED - should show all_images.images

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% write out the pulses
% NOT IMPLEMENTED
