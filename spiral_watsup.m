addpath util/
addpath util/rf_tools/
addpath util/rf_tools/mex5/

dataPath = '~/data/ktpoints_watsup/';

mapselect = 'vandyInVivo,101415'; % B1+/B0 map selection
slind = 2; % index of slice - only works for 'janni_*'
flip = 90; % flip angle in deg
bw = 220; % Hz, width of passband
totalDur = 26; % ms, duration of pulse
subpulseDur = 1/(5.2*7*42.58)*1000; % 5.2 ppm spectral separation between repeated passbands
NSubPulse = floor(totalDur/subpulseDur)+1;
d1 = 0.001; % passband ripple (this is the target % flip angle error in water)
d2 = 0.01; % stopband ripple (this is the target % flip angle error outside of water)
dt = 6.4e-6; % seconds, dwell time
fullDesign = false; % design the entire pulse all at once, or separate it in spatial and spectral dims
powReg = 10^-1; % RF power regularization parameter (-1 for 10/14/15)
dimxy = [32 32]; % dimensions of of design grid in (x,y,z)

trajType = 'meas'; % options: 'in', 'inout', 'outin', 'meas'
kPhsInc = 0; % phase increment between consecutive spirals (only compatible with full design)
FOVsubsamp = 10; % cm, xfov of spiral (10 is best for phantom, 12 for vandy brain, 10 for janni_ang/pfc)
dgdtmax = 18000; % slew (g/cm/s)
gmax = 4; % g/cm

% Parameters for saturated acquisition
crushAmp = 2; % gauss/cm, amp of crusher for saturation acq
crushDur = 5; % ms, dur of crusher for saturation acq
delDur = 1; % ms, delay between pulses in sat sequence
hardPulseDur = 0.5; % ms, duration of 90 hard EX pulse after crusher

newMask = false; % let the user select a new mask
simFreqs = -1500:50:1500; % Hz, range of frequencies to simulate the subpulse over in the separable design
NfSim = length(simFreqs);

rootFlipEnvelope = true; % switch to root flip the spectral envelope

useGmri = false; % switch to use Gmri or brute-force construction

% Gmri off-resonance settings
L = 4; % number of time segments
NGmri = 1; % number of Gmri objects
ncgiters = 5; % number of conjugate gradient iterations per spatial pulse update

% calculate spectral time-bandwidth product
tbw = bw*totalDur/1000; % time-bandwidth product

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load b1/b0 maps
switch mapselect
    case 'vandyPhantom,101415'
        tmp = load([dataPath '101415_phantom/3dfullb1b0.mat']);
        Nc = 1;
        maps.b1 = tmp.b1;
        maps.b0 = tmp.b0;
        maps.mask = tmp.mask;
        maps.info = tmp.info;
        if newMask
            mask = maps.mask.*roiSelect(maps.b1.*maps.mask,'ellipse','jet');
            save([dataPath '101415_phantom/vandyPhantom101415Mask'],'mask');
        else
            load([dataPath '101415_phantom/vandyPhantom101415Mask']);
        end
        maps.mask = mask;
    case 'vandyInVivo,100715'
        maps = load([dataPath '100715_invivo/3dfullb1b0.mat']);
        Nc = 1;
        if newMask
            mask = maps.mask.*roiSelect(maps.b1.*maps.mask,'freehand','jet');
            save([dataPath '100715_invivo/vandy100715InVivoMask'],'mask');
        else
            load([dataPath '100715_invivo/vandy100715InVivoMask']);
        end
        maps.mask = mask;
    case 'vandyInVivo,101415'
        maps = load([dataPath '101415_invivo/3dfullb1b0.mat']);
        Nc = 1;
        if newMask
            mask = maps.mask.*roiSelect(maps.b1.*maps.mask,'freehand','jet');
            save([dataPath '101415_invivo/vandy101415InVivoMask'],'mask');
        else
            load([dataPath '101415_invivo/vandy101415InVivoMask']);
        end
        maps.mask = mask;
end


maps.b0 = -maps.b0; % opposite dir of precession compared to GE

fovx = maps.info.imgdef.pixel_spacing_x_y.uniq(1) * size(maps.mask,1) / 10; % cm
fovy = maps.info.imgdef.pixel_spacing_x_y.uniq(2) * size(maps.mask,2) / 10;
fovz = 0;
fov = [fovx fovy];
%fovz = abs(info.imgdef.slice_gap_in_mm.uniq) * size(mask,3) / 10;

offcentre = mean(maps.info.imgdef.image_offcentre_ap_fh_rl_in_mm.uniq/10,1); % center in cm

% interpolate the B1 and B0 maps to the target grid
% define output interpolation grid
[xo,yo] = meshgrid(-fov(1)/2:fov(1)/dimxy(1):fov(1)/2-fov(1)/dimxy(1),...
    -fov(2)/2:fov(2)/dimxy(2):fov(2)/2-fov(2)/dimxy(2));

% interpolate B1 maps
[xi,yi] = meshgrid(-fov(1)/2:fov(1)/size(maps.b1,1):fov(1)/2-fov(1)/size(maps.b1,1),...
    -fov(2)/2:fov(2)/size(maps.b1,2):fov(2)/2-fov(2)/size(maps.b1,2));
for ii = 1:Nc
    sens(:,:,ii) = interp2(xi,yi,maps.b1(:,:,ii).*maps.mask,xo,yo,'linear',0);
end

% interpolate B0 map
[xi,yi] = meshgrid(fov(1)*[-1/2:1/size(maps.b0,1):1/2-1/size(maps.b0,1)],...
    fov(2)*[-1/2:1/size(maps.b0,2):1/2-1/size(maps.b0,2)]);
fmap = interp2(xi,yi,maps.b0.*maps.mask,xo,yo,'linear',0);

% get interpolated mask - nonzero where interpolated sens's are nonzero,
% since we already masked sens's prior to interpolation
mask = sqrt(sum(abs(sens).^2,3)) > 0;

fmax = 250;
mask(fmap < -fmax | fmap > fmax) = false;
sens(fmap < -fmax | fmap > fmax) = 0;
fmap(fmap < -fmax | fmap > fmax) = 0;

% try a spiral
deltax = 1; % spatial resolution of trajectory, cm

forwardspiral = 0;
dorevspiralramp = 0;
traj = 'spiral';
get_traj;

switch trajType
    case 'meas'
        
        % spiral in-out, measured and grip'd
        traj = load('~/data/ktpoints_watsup/100315_kspacecal/final.mat');
        g1 = traj.g_grip_sameArea(:,:,end);
        k1 = traj.k_meas;k1 = k1 - repmat(k1(50,:),[100 1]); % set to DC in middle
        rfmask1 = traj.designvars.rfmask1;
        
    case 'inout'
        
        % spiral in-out (each one is 1/2 subpulse duration)
        
        % strip off samples until we get just under subpulsedur/2,
        % accounting for ramp
        g = g(end-ceil(subpulseDur/2/(dt*1000))+1:end,:);
        k = k(end-ceil(subpulseDur/2/(dt*1000))+1:end,:);
        nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        while (size(g,1) + nRamp)*dt*1000 > subpulseDur/2
            g = g(2:end,:);
            k = k(2:end,:);
            nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        end
        gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
        g = [gRamp;g];
        g1 = [g; -flipud(g)];
        
        rfmask = [false(nRamp,1);true(size(k,1),1)];
        k = [zeros(nRamp,2);k];
        k1 = [k; flipud(k)];
        rfmask1 = [rfmask; flipud(rfmask)];

    case 'outin'
        
        % spiral out-in (each one is 1/2 subpulse duration)
        
        % strip off samples until we get just under subpulsedur/2,
        % accounting for ramp
        g = g(end-ceil(subpulseDur/2/(dt*1000))+1:end,:);
        k = k(end-ceil(subpulseDur/2/(dt*1000))+1:end,:);
        nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        while (size(g,1) + nRamp)*dt*1000 > subpulseDur/2
            g = g(2:end,:);
            k = k(2:end,:);
            nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        end
        gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
        g = [gRamp;g];
        g1 = [flipud(g);-g];
        
        rfmask = [false(nRamp,1);true(size(k,1),1)];
        k = [zeros(nRamp,2);k];
        k1 = [flipud(k);k];
        rfmask1 = [rfmask; flipud(rfmask)];
                
    case 'in'
        % spiral in only
        
        % strip off samples until we get just under subpulsedur/2,
        % accounting for ramp down AND rewinder
        g = g(end-floor(subpulseDur/(dt*1000))+1:end,:); 
        k = k(end-floor(subpulseDur/(dt*1000))+1:end,:);
        nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
        for ii = 1:2
            nTrap(ii) = length(dotrap(sum([gRamp(:,ii);g(:,ii)])*dt,gmax,dgdtmax,dt));
        end
        while (size(g,1) + nRamp + max(nTrap))*dt*1000 > subpulseDur
            g = g(2:end,:);
            k = k(2:end,:);
            nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
            gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
            for ii = 1:2
                nTrap(ii) = length(dotrap(sum([gRamp(:,ii);g(:,ii)])*dt,gmax,dgdtmax,dt));
            end
        end
        gTrap = {};
        for ii = 1:2
            gTrap{ii} = -dotrap(sum([gRamp(:,ii);g(:,ii)])*dt,gmax,dgdtmax,dt);
        end
        maxNTrap = max(nTrap);
        g1 = [[zeros(maxNTrap-length(gTrap{1}),1);gTrap{1}(:)] [zeros(maxNTrap-length(gTrap{2}),1);gTrap{2}(:)];gRamp;g];
        
        rfmask1 = [false(nRamp+maxNTrap,1);true(size(k,1),1)];
        k1 = [zeros(nRamp+maxNTrap,2);k];
        
end


% build up full gradient/k trajectories
kPhs = (0:floor(totalDur/subpulseDur))*kPhsInc;
k = [];
g = [];
for ii = 1:length(kPhs)
    k = [k; (k1(:,1) + 1i*k1(:,2))*exp(1i*kPhs(ii))];
    g = [g; (g1(:,1) + 1i*g1(:,2))*exp(1i*kPhs(ii))];
end
k = [real(k) imag(k)];
g = [real(g) imag(g)];
rfmask = repmat(rfmask1,[floor(totalDur/subpulseDur)+1 1]);

Nt = size(k,1);


if fullDesign

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % set up the target pattern as a function of frequency
    w = dinf(d1,d2)/tbw; % fractional transition width
    fedges = [(1-w)*tbw/2 (1+w)*tbw/2]/totalDur*1000; % frequency edges in Hz, normalized to Nyquist
    % set up oversampled design freq grid
    nf = 8*ceil(totalDur/subpulseDur); % oversample the frequency grid by a factor of 8, compared to subpulse frequency
    fb = (-nf:nf-1)/(nf)/(subpulseDur/1000); % frequencies in Hz
    % remove frequencies outside our ROI
    fmask = ((fb > -fedges(1) & fb < fedges(1)) | fb > fedges(2)) & (fb < (1/(subpulseDur/1000))-fedges(2));
    fbm = fb(fmask);
    
    % set up vector of target amplitudes
    famps = double(fb > -fedges(1) & fb < fedges(1));
    fampsm = famps(fmask);
    
    % passband/stopband ripple weights (REDFLAG: not yet implemented!!)
    fwts = double(fb > -fedges(1) & fb < fedges(1)) + d1/d2*double(fb > fedges(2));
    fwtsm = fwts(fmask);
    
    ti = ((0:Nt-1) - Nt)*dt; % time points vector
    k(:,3) = ti;
    mask3 = repmat(mask,[1 1 length(fb)]);
    wts3 = repmat(double(mask),[1 1 length(fb)]);
    for ii = 1:length(fb)
        mask3(:,:,ii) = mask3(:,:,ii) & fmask(ii);
        d3(:,:,ii) = mask3(:,:,ii) * famps(ii) * double(fmask(ii));
        wts3(:,:,ii) = double(mask3(:,:,ii) & fmask(ii))*fwts(ii);
    end
    d3 = d3 * flip*pi/180 / (2*pi*4258*dt); % scale to target flip and account for RF integral terms
    nufft_args = {size(mask3), [6 6 6], 2*size(mask3), size(mask3)/2, 'table', 2^10, 'minmax:kb'}; % NUFFT arguments
    b1 = repmat(sens,[1 1 length(fb)]);
    if NGmri == 1
        A = Gmri_SENSE(k,mask3,'fov',[fovx fovy 2/(subpulseDur/1000)],'sens',conj(b1(mask3)),'ti',ti,...
            'zmap',1i*2*pi*repmat(fmap,[1 1 length(fb)]),'L',L,'nufft_args',nufft_args)';
    else
        % use multiple Gmri objects stacked end-to-end
        NtPerSeg = ceil(Nt/NGmri);
        for ii = 1:NGmri
            inds = (ii-1)*NtPerSeg+1:min(ii*NtPerSeg,Nt);
            A{ii} = Gmri_SENSE(k(inds,:),mask3,'fov',[fovx fovy 2/(subpulseDur/1000)],'sens',conj(b1(mask3)),'ti',ti(inds),...
                'zmap',1i*2*pi*repmat(fmap,[1 1 length(fb)]),'L',L,'nufft_args',nufft_args)';
        end
        A = block_fatrix(A,'type','row');
    end
    ncgiters = 5;
    phs = 0*d3;
    rf = zeros(size(A,2),1);
    for ii = 1:20
        rf = qpwls_pcg(rf(:,end),A,diag_sp(wts3(mask3)),d3(mask3),0,10^12*diag_sp(double(~rfmask)),1,ncgiters,mask3); % CG
        rf = rf(:,end);
        m = 2*pi*4258*dt*embed(A*rf,mask3);
        phs = angle(m);
        d3 = abs(d3).*exp(1i*phs);
        norm(sqrt(wts3(mask3)).*(d3(mask3)*2*pi*4258*dt-m(mask3)))^2
    end

else % separable design (preferred)
    
    % first design a spatial pulse
    ti = ((0:length(k1)-1) - length(k1))*dt;
    if useGmri
        nufft_args = {size(mask), [6 6], 2*size(mask), size(mask)/2, 'table', 2^10, 'minmax:kb'}; % NUFFT arguments
        A = Gmri_SENSE(k1,mask,'fov',[fovx fovy],'sens',conj(sens(mask)./median(abs(sens(mask)))),'ti',ti,...
                        'zmap',1i*2*pi*fmap,'L',L,'nufft_args',nufft_args)';
        R = diag_sp(sqrt(powReg*sum(mask(:))/sum(rfmask1))*double(rfmask1)+10^12*double(~rfmask1));
    else
        [x,y] = ndgrid(-fovx/2:fovx/size(mask,1):fovx/2-fovx/size(mask,1),...
                       -fovy/2:fovy/size(mask,1):fovy/2-fovy/size(mask,2));
        A = exp(1i*2*pi*((x(mask(:))*k1(:,1).' + y(mask(:))*k1(:,2).') + ...
                fmap(mask(:))*ti(:)'));
        A = bsxfun(@times,A,sens(mask)./median(abs(sens(mask))));
        R = diag(sqrt(powReg*sum(mask(:))/sum(rfmask1))*double(rfmask1)+10^12*double(~rfmask1));
        Ainv = (A'*A + R'*R)\A';
    end

    d = mask;
    % set initial target phase as 
    phs = 0*d;
    rf1 = zeros(size(A,2),1);
    cost = [Inf norm(double(d(mask)))^2];
    % build regularization matrix to force zero RF on ramps and penalize
    % integrated power. Also normalize by # spatial locs and # RF time
    % points.
    while cost(end) < 0.9999*cost(end-1)
        if useGmri
            rf1 = qpwls_pcg(rf1(:,end),A,1,d(mask),0,R,1,ncgiters,mask); % CG
            rf1 = rf1(:,end);
        else
            rf1 = Ainv*d(mask);
        end
        m = embed(A*rf1,mask);
        phs = angle(m);
        d = abs(d).*exp(1i*phs); % MLS phase update
        res = d(mask)-m(mask);reg = R*rf1;
        cost(end+1) = real(res'*res) + real(reg'*reg);
        fprintf('Iteration %d cost: %d\n',length(cost),cost(end));
    end
    % let user know how the RF power and error terms balance out
    rfPowReg = R*rf1;rfPowReg = real(rfPowReg'*rfPowReg);
    exErr = A*rf1 - d(mask);exErr = real(exErr'*exErr);
    fprintf('At exit, error is %d and power reg is %d\n',exErr,rfPowReg);
    
    % simulate over wide frequency range
    disp('Simulating one-segment pulse over wide frequency range')
    mOneSegFreqs = [];
    errOneSegFreqs = [];
    for ii = 1:NfSim
        tmp = embed(A*(exp(1i*2*pi*ti(:)*simFreqs(ii)).*rf1),mask);
        errOneSegFreqs(ii) = norm(abs(d(mask))-abs(tmp(mask)))^2;
        mOneSegFreqs(:,:,ii) = tmp;
    end
    rf1 = rf1./(2*pi*4258*dt); % scale to target flip (1 radian)
    rf1 = rf1./median(abs(sens(mask))); % normalize by median of B1 map to 
                                        % improve regularization
                                        % consistency

    % get min-phase SLR pulse weights
    disp('Designing SLR envelope')
    [rfw,b] = dzrf(NSubPulse,tbw,'sat','min',0.001,0.01);
    rfw = real(rfw);
    
    if rootFlipEnvelope
        
        disp('Root-flipping SLR envelope')

        % root flip the envelope to minimize peak B1/SAR
        b = b./max(abs(freqz(b)));
        d1 = 0.001/2;
        b = b*sin(pi/2/2 + atan(d1*2)/2); % scale to target flip angle
        r = roots(b); % calculate roots of single-band beta polynomial
        % sort the roots by phase angle
        [~,idx] = sort(angle(r));
        r = r(idx);r = r(:);
        r = leja_fast(r);
        
        candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tbw/length(b)*pi;% ...

        rfall = [];
        for ii = 1:2^sum(candidates)
            
            % convert this ii to binary pattern
            doflip = de2bi(ii-1);
            % add zeros for the rest of the passbands
            doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
            % embed in all-roots vector
            tmp = zeros(length(b)-1,1);
            tmp(candidates) = doflip;
            doflip = tmp;
            % flip those indices
            rt = r(:);
            rt(doflip == 1) = conj(1./rt(doflip == 1));
            % get polynomial coefficients back from flipped roots
            tmp = poly(rt);
            %tmp = interp1(0:1/(N-1):1,tmp(:).',0:1/(nt-1):1,'spline',0);
            % important: normalize before modulating! doesn't work
            % for negative shifted slices otherwise
            tmp = tmp./max(abs(freqz(tmp)));
            % store the rf
            rfall = [rfall col(b2rf(tmp(:)*sin(pi/2/2 + atan(d1*2)/2)))];
            
        end
        
        % take RF with min peak amplitude
        [~,ind] = min(max(abs(rfall),[],1));
        rfw = rfall(:,ind);
        
    end
    
    % apply the weights to build the whole pulse
    rf = kron(rfw(:),rf1(:));
    
end


% Bloch-simulate over entire frequency range
disp('Bloch-simulating entire pulse')
k = repmat(k1,[NSubPulse 1]);
Nt = size(k,1);
ti = ((0:Nt-1) - Nt)*dt;
k(:,3) = ti;
omdt = repmat(fmap,[1 1 NfSim])*2*pi*dt;
omdt = omdt(repmat(mask,[1 1 NfSim]));
[xx,yy,ff] = ndgrid(-fov(1)/2:fov(1)/dimxy(1):fov(1)/2-fov(1)/dimxy(1),...
     -fov(2)/2:fov(2)/dimxy(2):fov(2)/2-fov(2)/dimxy(2),...
     simFreqs);
xx = [xx(repmat(mask,[1 1 NfSim])) yy(repmat(mask,[1 1 NfSim])) ff(repmat(mask,[1 1 NfSim]))];
sensBloch = repmat(sens,[1 1 NfSim]);sensBloch = complex(sensBloch(repmat(mask,[1 1 NfSim])));
garea = [zeros(1,3);diff(k(:,1:3),1,1)/dt/4258]*2*pi*dt*4258;
[a,b] = blochsim_optcont_mex(ones(Nt,1),ones(Nt,1),rf(:)*dt*4258*2*pi,sensBloch,...
                             garea,xx,omdt,2);
Mxy = embed(2*a.*b,repmat(mask,[1 1 NfSim]));
Mz = embed(1-2*abs(b).^2,repmat(mask,[1 1 NfSim]));

disp('Writing waveforms')

% negate the y gradient
g(:,2) = -1*g(:,2);

% write RF to file
rf_am = abs(rf);
bsrf = load('~/code/bsrfopt/bsrf_21-Mar-2012.mat'); % read the target nominal b1 value from this file
rfang = sum(rf_am)*dt*bsrf.nomb1*4257*2*pi*180/pi;
%tipangle = sum(4258*2*pi*180/pi*rf_am*dt);
rf_fm = diff([0;unwrap(angle(rf))])/dt/2/pi;% this is probably the better one
rf2ppe('tailored_ex_rf.txt.spiral_watsup',1000*dt*length(rf_am),rfang,1000*dt*length(rf_am),0,rf_am,rf_fm);
rf2ppe_watsup('tailored_watsup_rf.txt.spiral_watsup',tbw,1000*dt*length(rf_am),rfang,1000*dt*length(rf_am),0,rf_am,rf_fm);


if ~fullDesign
    % also write just one of them
    rf1_am = abs(rf1);
    rfang = sum(rf1_am)*dt*bsrf.nomb1*4257*2*pi*180/pi;
    tipangle = sum(4258*2*pi*180/pi*rf1_am*dt);
    rf1_fm = diff([0;unwrap(angle(rf1))])/dt/2/pi;% this is probably the better one
    rf2ppe('tailored_ex_rf.txt.spiral_watsup_oneRF',1000*dt*length(rf1_am),tipangle,1000*dt*length(rf1_am),0,rf1_am,rf1_fm);
end
% write entire gradient
gr2ppe('tailored_ex_grads.txt.spiral_watsup',1000*dt*size(g,1),1000*dt*size(g,1),10*[g zeros(size(g,1),1)]);
gr2ppe('tailored_watsup_grads.txt.spiral_watsup',1000*dt*size(g,1),1000*dt*size(g,1),10*[g zeros(size(g,1),1)]);

% write single subpulse gradient
gr2ppe('tailored_ex_grads.txt.spiral_watsup_oneRF',1000*dt*size(g1,1),1000*dt*size(g1,1),10*[g1 zeros(size(g1,1),1)]);

% write out pulses for saturation acquisition
gCrush = dotrap(crushAmp*crushDur/1000,crushAmp,dgdtmax,dt); % the crusher gradient
gCrush = gCrush(:); nCrush = length(gCrush);
nHard = ceil(hardPulseDur/dt/1000); % # samples in hard ex pulse
% scale hard pulse to average 90 flip
a = 1/(2*pi*4258*dt*nHard)*(abs(sens(mask))'*abs(sens(mask)))^-1*sum(abs(sens(mask)))*pi/2;
rfHard = ones(nHard,1)*a; % 90 hard pulse
nDel = ceil(delDur/dt/1000); % # samples in delay
rf_am_crush = [rf_am;zeros(2*nDel+nCrush,1);rfHard];
rf_am_crush_zero = [0*rf_am;zeros(2*nDel+nCrush,1);rfHard];
rf_fm_crush = [rf_fm;zeros(2*nDel+nCrush+nHard,1)];
gCrushAll = 10*[[g;zeros(nCrush+2*nDel+nHard,2)] ...
    [zeros(size(g,1),1);zeros(nDel,1);gCrush(:);zeros(nDel+nHard,1)]];
rfang = sum(rf_am_crush)*dt*bsrf.nomb1*4257*2*pi*180/pi;
rf2ppe('tailored_ex_rf.txt.spiral_watsup_sat',1000*dt*length(rf_am_crush),...
    rfang,1000*dt*length(rf_am_crush),0,rf_am_crush,rf_fm_crush);
rfang = sum(rf_am_crush_zero)*dt*bsrf.nomb1*4257*2*pi*180/pi;
rf2ppe('tailored_ex_rf.txt.spiral_watsup_sat_norf',1000*dt*length(rf_am_crush_zero),...
    rfang,1000*dt*length(rf_am_crush_zero),0,rf_am_crush_zero);
gr2ppe('tailored_ex_grads.txt.spiral_watsup_sat',1000*dt*size(gCrushAll,1),...
    1000*dt*size(gCrushAll,1),gCrushAll);

% save workspace for gradient pre-emphasis
save rfworkspace

