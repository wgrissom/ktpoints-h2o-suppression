%% 30-9-13: Distort gradients to predict effect of 43us eddy current term
%%% Function also makes transmit and receive kspaces 

function [Geddy,ktx,krx] = gradient_distort(G,tau,delay,dt)

if numel(tau)<3
    tau = tau(1)*[1 1 1]; % allow channel dependent tau
end

if ~exist('delay','var')
    delay=tau;
end
if ~exist('dt','var')
    dt=6.4e-6;
end
    
%%% add some zeros on front and end
Npad=10;
G = cat(1,zeros([Npad 3]),G,zeros([Npad 3]));
M = length(G);
t = (0:M-1)*dt;

Geddy = zeros([M 3]);

for ix=1:3
    tmp = (1/tau(ix))*exp(-t(:)/tau(ix)).*cumtrapz(exp(t(:)/tau(ix)).*G(:,ix))*dt;
    % pull forwards: scanner does this
    Geddy(:,ix) = interp1(t,tmp,t+delay(ix));
    %HaHo: remove if no eddy current correction & delay is wanted
%     fprintf(1,'No eddy current correction!!!!\n');
%     Geddy = G;
end
Geddy(isnan(Geddy))=0;

%%% Now make k-spaces before removing the padding
gamma_mT = 2*pi*42577.46778; % rad s^-1 mT^-1

%%% Tx - integrate to end
ktx = -gamma_mT*flipud(cumtrapz(t,flipud(Geddy)));
%%% Rx - simple integration as you go along
krx = gamma_mT*cumtrapz(t,Geddy);

%%% remove padding
Geddy([1:Npad (M-Npad+1):M],:)=[];
ktx([1:Npad (M-Npad+1):M],:)=[];
krx([1:Npad (M-Npad+1):M],:)=[];
end