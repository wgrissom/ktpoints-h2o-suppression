% load the b1 data
function mask = roiSelect(img,maskType,cMapType)

if nargin < 2
    maskType = 'freehand';
end
if nargin < 3
    cMapType = 'gray';
end

mask = false(size(img));
for ii  = 1:size(img,3)
    figure; imagesc(img(:,:,ii));
    axis image; colormap(cMapType);
    switch maskType
        case 'ellipse'
            title('ELLIPSE TOOL');
            h = imellipse;
            vert_elps = wait(h);
            mask(:,:,ii) = poly2mask(vert_elps(:,1),vert_elps(:,2), size(img,1), size(img,2)) > 0;
        case 'freehand'
            title('FREEHAND TOOL');
            h = imfreehand;
            vert_free = wait(h);
            mask(:,:,ii) = poly2mask(vert_free(:,1),vert_free(:,2), size(img,1), size(img,2)) > 0;
    end
end
